#!/usr/bin/python3
# -*- coding: utf-8 -*-
from xml.sax import make_parser
import sys
import json
import urllib.request
import smallsmilhandler


class karaokeLocal():
    def __init__(self, fich):
        self.fich = fich
        parser = make_parser()
        cHandler = smallsmilhandler.SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(self.fich))
        self.lista = cHandler.get_tags()

    def __str__(self):
        texto = ""
        for elementos in self.lista:
            for etiqueta in elementos:
                if etiqueta == "etiqueta":
                    texto += elementos[etiqueta] + "\t"
                elif elementos[etiqueta] != "":
                    texto += elementos[etiqueta] + "\t"
            texto += "\n"
        return texto

    def do_json(self):
        # para darle el numero del fichero acabado en json
        with open(self.fich.split(".")[0] + ".json", "w") as f:
            json.dump(self.lista, f, indent=2)
            # indent = 2 es para que no lo saque en una unica linea

    def do_local(self):
        for elementos in self.lista:
            for etiqueta in elementos:
                if etiqueta == "src":
                    url = elementos[etiqueta]
                    if url[0:7] == "http://":
                        fich = url.split("/")[-1]
                        urllib.request.urlretrieve(url, fich)
                        elementos[etiqueta] = fich


if __name__ == "__main__":
    """
    Programa principal
    """
    try:
        ficher = sys.argv[1]
    # muy importante el orden: primer parser y luego chandler
    except IndexError:
        print("Usage: python3 karaoke.py file.smil")

    etiquetax = karaokeLocal(ficher)
    etiquetax.do_local()
    etiquetax.do_json()
    print(etiquetax)
