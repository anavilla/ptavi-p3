#!/usr/bin/python3
# -*- coding: utf-8 -*-
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


# SamllSMILHandler hereda de ContentHandler
class SmallSMILHandler(ContentHandler):
    """
    Clase para manejar smil
    """

    # Siempre def init al ejecutar clases, solo pasamos el parametro self (propio)

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        # self.atributo = valor (Se hacen globables)
        self.width = ""
        self.height = ""
        self.backgroundcolor = ""
        self.id = ""
        self.top = ""
        self.bottom = ""
        self.left = ""
        self.right = ""
        self.src = ""
        self.region = ""
        self.begin = ""
        self.dur = ""
        # Creacion de lista vacia
        self.lista = []

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'root-layout':
            # De esta manera tomamos los valores de los atributos
            self.width = attrs.get('width', "")
            self.height = attrs.get("height", "")
            self.backgroundcolor = attrs.get("background-color", "")
            listaroot = {"etiqueta": name, "width": self.width,
                         "height": self.height,
                         "backgroundcolor": self.backgroundcolor}
            self.lista.append(listaroot)
        elif name == "region":
            self.id = attrs.get("id", "")
            self.top = attrs.get("top", "")
            self.bottom = attrs.get("bottom", "")
            self.left = attrs.get("left", "")
            self.right = attrs.get("right", "")
            listaregion = {"etiqueta": name, "id": self.id,
                           "top": self.top, "bottom": self.bottom,
                           "left": self.left, "right": self.right}
            self.lista.append(listaregion)

        elif name == 'img':

            self.src = attrs.get('src', "")
            self.region = attrs.get('region', "")
            self.begin = attrs.get('begin', "")
            self.dur = attrs.get('dur', "")
            self.end = attrs.get('end', "")
            listaimg = {'etiqueta': name, 'src': self.src,
                        'region': self.region, 'begin': self.begin,
                        'dur': self.dur, 'end': self.end}
            self.lista.append(listaimg)

        elif name == 'audio':

            self.src = attrs.get('src', "")
            self.begin = attrs.get('begin', "")
            self.dur = attrs.get('dur', "")
            listaudio = {'etiqueta': name, 'src': self.src,
                         'begin': self.begin, 'dur': self.dur}
            self.lista.append(listaudio)

        elif name == 'textstream':
            self.src = attrs.get('src', "")
            self.region = attrs.get('region', "")
            self.fill = attrs.get('fill', "")
            listatext = {'etiqueta': name, 'src': self.src,
                         'region': self.region, 'fill': self.fill}
            self.lista.append(listatext)

    def get_tags(self):
        return self.lista


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    lista = cHandler.get_tags()
    print(lista)
